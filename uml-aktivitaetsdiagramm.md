## UML Aktivitätsdiagramm

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-aktivitaetsdiagramm</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109865194039578226</span>

> **tl/dr;** _(ca. 6 min Lesezeit): UML-Aktivitätsdiagramme sind die Flussdiagramme der UML zur Planung und Dokumentation von Algorithmen und Abläufen. In diesem Artikel geht es um die Grundlagen dieses Diagramms (Abläufe, Parallelisierung, Swimlanes). In einem zweiten Teil wird es um Feinheiten wie den Objektfluss und Umgang mit Exceptions gehen..._


Dieser Artikel ist Teil einer Artikelreihe über UML-Diagramme: [Klassendiagramm](https://oer-informatik.de/uml-klassendiagramm) / [Sequenzdiagramm](https://oer-informatik.de/uml-sequenzdiagramm) / [Aktivitätsdiagramm](https://oer-informatik.de/uml-aktivitaetsdiagramm) / [Zustandsdiagramm (State)](https://oer-informatik.de/uml-zustandsdiagramm) / [Anwendungsfall-Diagramm (Use Case)](https://oer-informatik.de/uml-usecase). Für alle sind jeweils auch PlantUML-Anleitungen verlinkt. Zu Aktivitätsdiagrammen existieren Artikel zum [Entwurf digitalisierter Geschäftsprozesse mithilfe des Aktivitätsdiagramms](https://oer-informatik.de/geschaeftsprozess-digitalisieren-und-optimieren), [Übungsaufgaben zum Aktivitätsdiagramm](https://oer-informatik.de/uml-aktivitaetsdiagramm-uebung-webapp) und [Hinweise zur Erzeugung von Diagrammen mit PlantUML](https://oer-informatik.de/uml-aktivitaetsdiagramm-plantuml).

<span class="kann-liste">

Nach Durchlesen des Artikels über UML Aktivitätsdiagramme kann ich...

- ... Startknoten, Ablaufende und Endknoten notieren,

- ... Entscheidungen mit Bedingungen (_guards_) bezeichnen und mit Zusammenführungen wieder vereinen,

- ... parallele Strukturen mit Gabelung und Synchronisierung notieren,

- ... Aktionen in Aktivitäten gruppieren,

- ... neben Kontrollflüssen auch Objektflüsse darstellen (über Pin- Notation und mit Objektknoten),

- ... korrekte Pfeile und Symbole verwenden,

- ... Verantwortungsbereiche über Partitionen (Swimlanes) darstellen,

- ... gesendete und empfangene Signale darstellen,

- ... strukturierte Knoten verwenden (Alternativen, Schleifen),

- ... Parametersätze notieren,

- ... Tokentunnel darstellen,

- ... Zeitereignisse darstellen und

- ... Vor- und Nachbedingungen von Aktivitäten darstellen.

</span>

### Diagramm zur Verhaltensmodellierung

Ein UML-Aktivitätsdiagramm (*Activitydiagram*) beschreibt, wie das Verhalten eines (Software)-System *realisiert* ist. Es erweitert die Ausdrucksmöglichkeiten gegenüber einfacheren Flussdiagrammen wie dem Programmablaufplan z.B. durch die Möglichkeit Parallelisierung, Ausnahmen oder Datenflüsse abzubilden. Im Aktivitätsdiagramm können Daten- und Kontrollflüsse modelliert werden, z.B. von

-   Algorithmen,

-   Geschäftsprozessen oder

-   Workflows.

Im Fokus steht die Modellierung der Abfolge (sequenziell oder parallel), Bedingungen, Verzweigungen, Wiederholungen sowie Anfang und Ende von Aktivitäten. Alle Schritte können zudem in Verantwortungsbereichen konkreten Akteur*innen oder Systemen zugewiesen werden.

### Das Tokenmodell im Flussdiagramm: mit Knoten und Kanten

Aktivitätsdiagramme bestehen aus **Knoten** und **Kanten**.
Sie nutzen das Tokenmodell: ein *Token* ist eine gedachte Markierung. Diese kennzeichnet die momentan ausgeführten Aktionen - vergleichbar mit dem springenden Punkt beim Karaokesingen, der die aktuelle Silbe markiert. Ein Token wird im Startpunkt (*initial node*) erzeugt und durchläuft das Diagramm entlang der gerichteten Kanten (Pfeile, *edges*). Im Endpunkt (*activity final node*) wird der Token konsumiert und die Aktivität so beendet.

Ein minimales  Aktivitätsdiagramm, das eine unverzweigte Aktivität darstellt, sieht wie folgt aus:

![UML Aktivitätsdiagramm - Start - Schritt 1 - Schritt 2 -Stop](plantuml/01_aktion_start_stop.png)

### Kontrollstrukturen: Verzweigung und Vereinigung

Neben dem rein sequenziellen unverzweigten Ablauf können auch bedingte Anweisungen modelliert werden. Verzweigungen werden als Entscheidungsknoten (*decision node*) mit dem Raute-Symbol notiert. Die Bedingungen selbst werden als *guard* bezeichnet und  in eckigen Klammern an der jeweiligen Kante notiert. (Die Notation wird häufig mit dem Programm-Ablaufplan verwechselt, bei dem die Bedingung in die Raute und die Alternativen ohne eckige Klammern geschrieben wird.)

Die Bedingungen müssen als bool'scher Ausdruck formuliert sein - also mit *true* oder *false* auswertbar sein. Sie sollten *disjunkt* sein (sich gegenseitig ausschließen), um ein vorhersagbares Verhalten zu modellieren. Bei einfachen Verzweigungen erreicht man das, in dem zu jeder Bedingung ein `[else]`-Zweig modelliert wird.

![Verzweigungen mit einfacher Bedingung im UML Aktivitätsdiagramm](plantuml/03_einfacheBedingung.png)


Im Gegensatz zum Programmablaufplan werden die verschiedenen Kanten einer Verzweigung auch wieder an einer Raute (*merge node*) zusammengeführt. Weder ein *decision node* noch ein *merge node* ändern die Anzahl der vorhandenen Token.

![Verzweigungen mit einfacher Bedingung im UML Aktivitätsdiagramm](plantuml/03_einfacheVerzweigung.png)

Nicht alle Bedingungen lassen sich unmittelbar als bool'scher Ausdruck (true/false) formulieren.
Sofern nähere Erläuterungen nötig sind, sieht die UML vor, dass diese über eine Notiz mit dem Stereotyp `<<decision input>>` erfolgt, die mit der *decision node* über eine gestrichelte Linie verbunden wird.

![Komplexere Verzweigungen im UML Aktivitätsdiagramm](plantuml/03_komplexereVerzweigungAngepasst.png)


Mehrfache Verzweigungen können durch mehr Kanten realisiert werden, die den *decision node* verlassen. Wichtig ist auch hier, dass die *guards* disjunkt sind. Die Zusammenführung muss auch in diesem Fall über *merge nodes* erfolgen - das ist in der folgenden Grafik nicht normkonform (weil es das verwendete Tool anders darstellt).

![Mehrfache Verzweigungen im UML Aktivitätsdiagramm - es fehlt die *merge-node*](plantuml/03_mehrfacheVerzweigungAngepasst.png)

### Wiederholungsstrukturen: kopf- und fussgesteuerte Schleifen

Eine nachgelagerte Bedingung, die eine Wiederholung bestimmter Aktionen erzwingt, wird über die Rückführung der ausgehenden Kante mit *guard* einer *decision node* modelliert. Bei diesen fußgesteuerten Schleifen werden die Aktionen innerhalb der Schleife in jedem Fall einmal ausgeführt:

![Fussgesteuerte Schleifen im UML Aktivitätsdiagramm](plantuml/04_fussgesteuerteSchleife.png)


Im Fall einer kopfgesteuerten Schleife wird die Bedingung zunächst geprüft, die zu wiederholenden Aktionen also ggf. nie ausgeführt:

![Kopfgesteuerte Schleifen im UML Aktivitätsdiagramm](plantuml/04_kopfgesteuerteSchleife.png)


### Concurrency: Parallelisierung von Aktionen (Splitting und Synchronisation)


Im Gegensatz zu einem *decision node* oder *merge node* müssen bei Gabelungen (*fork node*) oder Synchronisierung (*join node*) an allen eingehenden Kanten ein Token anliegen, damit sie wiederum Token weiterreichen. Entsprechend werden an allen ausgehenden Kanten dann Token weitergereicht. Auf diese Art werden parallele Prozesse und Synchronisierungen modelliert.

![Die Aktivitäten zwischen *fork node* oder *join node* ("Mache dies" und "mache gleichzeitig das") laufen parallel ab.](plantuml/05_concurrency.png)

Nach UML-Standard gilt diese Regel auch für alle Aktivitäten: sie werden erst ausgeführt, sobald an allen eingehenden Kanten ein Token anliegt - und sie feuern nach Abschluss der Aktivität an allen ausgehenden Kanten einen Token. Diese Festlegung wird in der Praxis sehr häufig missachtet, sodass sich im Internet und der Literatur zahlreiche nicht navigierbare Aktivitätsdiagramme finden.

### Partitionen / Swimlanes zur Unterscheidung von Verantwortungsbereichen

Die UML sieht vor, dass modelliert werden kann, welche Akteurin oder welches System für bestimmte Aktionen verantwortlich oder organisatorisch Zuständig ist. Aufgrund des Aussehens werden diese Verantwortungsbereiche, die die UML *partition* nennt, oft *swimlanes* genannt.

![Aktivitäten sind den Partitionen von "Geschäftsführer", "Fachabteilung" und "Kunde" zugeordnet.](plantuml/06_swimlanes.png)


### Signale/ Ereignisse senden und empfangen

Token können nicht nur aus einem Startknoten entspringen, sondern auch über Signale erzeugt werden, die in einer *accept event action* empfangen werden. Sofern dieser Signalempfängerknoten auch eingehende Kanten hat, wird der ausgehende Token erst gefeuert, wenn ein Token anliegt und ein Signal eingeht. Der Token wandert dann wie gewohnt entlang der Kanten von Knoten zu Knoten. Signale können auch gesendet werden. Beim Erreichen einer *send signal action* wird asynchron das Signal gesendet und die nächste Aktion bearbeitet. Es wird nicht auf Antwort oder Empfangsbestätigung gewartet.

![Der Post-Request wird als Signal von _Unbekannt_ empfangen, die Respond wird als Signal zu _Unbekannt_ gesendet](plantuml/07_signals.png)

### Objektflüsse darstellen

Im obigen Beispiel sind nicht nur Signale und Aktivitäten dargestellt: Sofern nicht der Kontrollfluss symbolisiert werden soll, sondern konkrete Daten, so wird ein Objektknoten als Rechteck notiert. An den ein- und ausgehenden Kanten wird anstelle eines abstrakten Tokens dann ein Objekt transportiert. _Message Header + Body_ sowie _Response_ sind solche Objekte (Daten), die von einer Aktivität als Ausgabewert übergeben und von einer anderen als Eingabewert (Parameter) entgegengenommen werden.

Die Darstellung kann nicht nur in der oben gezeigten Form (als Rechteck) erfolgen, sondern es können einzelne Parameter auch an einer normalen Aktivität notiert werden - über die sogenannte Pin-Notation.

### Ablaufende

Wenn das Erreichen eines Endes zwar den aktuellen Ausführungsstrang beendet - also den eingetroffenen Token konsumiert - aber in der Gesamtaktivität noch weitere Token vorhanden sein können, muss ein Ablaufende ()*activity final node*) modelliert werden. Im Gegensatz zu einem *activity final node* werden nicht alle vorhandenen Token konsumiert, sondern nur der am *activity final node* eintreffende Token.

![Tokensenke: der am _activity final node_ eintreffende Token wird konsumiert](plantuml/08_ablaufende.png)


### Konnektoren (Sprungmarken) zur übersichtlicheren Darstellung


Um Kreuzungen zu vermeiden oder um komplexere Zusammenhänge übersichtlicher darstellen zu können, können Sprungmarken (A) verwendet werden. Abläufe können dadurch unterbrochen und in gesonderten Aktivitätsdiagrammen ausgeführt werden.

![Die Sprungmarke A stellt einen Tokentunnel dar, der den aktiven Token in ein anderes Diagramm (oder an eine andere Stelle) führt.](plantuml/09_sprungmarken.png)


### Vor- und Nachbedingungen

Aktionen können mit Vor- und Nachbedingungen versehen werden. Diese werden jeweils als Notiz mit dem jeweiligen Stereotyp notiert:

-   `<<localPrecondition>>`: Diese Bedingungen müssen vor Eintritt in
    die Aktion erfüllt sein

-   `<<localPostcondition>>` : Diese Bedingungen müssen vor Beendigung
    der Aktion erfüllt sein

![Aktivität mit Vor- und Nachbedingung als Notiz](plantuml/10_vornachbedingung.png)

### Darstellung von Exceptions

Neben der Parallelisierung und den Objektflüssen ist ein weiterer Unterschied zwischen UML Aktivitätsdiagramm und Programm-Ablaufplan, dass im Aktivitätsdiagramm auch Ausnahmen (Exceptions) dargestellt werden können. An Stelle eines normalen Pfeils werden für geworfene Exceptions Blitz-Pfeile genutzt. Diese verlassen ein durch eine gestrichelte Grenze angedeutetes System.

### Fazit

In Teil eins über UML-Aktivitätsdiagramm ging es v.a. um die Standardnotationsmittel. In einem zweiten Teil wird der Objektfluss mit Parameter(-gruppen) und Bedingungen sowie das Exceptionhandling eine Rolle spielen.


Ein Beispiel für ein Aktivitätsdiagramm für einen bestehenden Prozess ist beispielsweise dieses, das eine Authentifizierung mit OAuth2 darstellt:

![UML-Aktivitätsdiagramm der Authentifizierung mit OAuth](plantuml/oauth-Activity-diagramm.png)


### Weitere Literatur zum UML-Aktivitätsdiagramm

- **als Primärquelle**: die UML Spezifikation der Object Management Group
Definition des Standards, jedoch nicht für Endnutzer aufbereitet): [https://www.omg.org/spec/UML](https://www.omg.org/spec/UML)

- **für den Einstieg**: Martina Siedl, Marion Brandsteidl, Christian Huemer, Gerti Kappel: UML@Classroom, dpunkt Verlag, Heidelberg 2012
Gut zu lesende Einführung in die wichtigsten UML-Diagramme, Empfehlung für den Einstieg.

- **für Lesefaule**: Die Vorlesungsreihe der Technischen Uni Wien (zu UML@Classroom) kann hier angeschaut werden (Videos, Folien): [http://www.uml.ac.at/de/lernen](http://www.uml.ac.at/de/lernen)

- **als Nachschlagewerk**: Christoph Kecher, Alexander Salvanos: UML 2.5 – Das umfassende Handbuch, Rheinwerk Bonn 2015, ISBN 978-3-8362-2977-7
Sehr umfangreiches Nachschlagewerk zu allen UML-Diagrammen

- **als Cheatsheet**: die Notationsübersicht von oose.de: [https://www.oose.de/wp-content/uploads/2012/05/UML-Notations%C3%BCbersicht-2.5.pdf](https://www.oose.de/wp-content/uploads/2012/05/UML-Notations%C3%BCbersicht-2.5.pdf)

- **UML und Software Engineering**: Chris Rupp, Stefan Queins & die SOPHISTen: UML2 glasklar, Hanser Verlag, München 2012; ISBN 978-3-446-43057-0 Schwerpunkt: Einbindung von UML-Diagrammen in den Softwareentwicklungszyklus

- **Zur Zertifizierungs-Vorbereitung**: M. Chonoles: OCUP 2 Certification Guide , Morgan Kaufmann Verlag, Cambridge 2018, ISBN 978-0-12-809640-6
    Informationen zur Zertifizierung nach OMG Certified UML Professional 2™ (OCUP 2™): Foundation Level

### Software zur Erzeugung von UML-Aktivitätsdiagrammen

- [PlantUML](http://www.plantuml.com): Deklaratives UML-Tool, das in vielen Entwicklungsumgebungen integriert ist. auch als WebEditor verfügbar. Die obigen Diagramme wurden damit erzeugt - [Eine detaillierte Anleitung für plantUml mit den Quelltexten findet sich hier](https://oer-informatik.gitlab.io/uml/umlsequenz/uml-sequenzdiagramm-plantuml.html)

- [WhiteStarUML](https://sourceforge.net/projects/whitestaruml/) (Relativ umfangreiches Tool, viele UML-Diagramme, mit Code-Generierung für Java, C, C# und Vorlagen für Entwurfsmuster)

- [Draw.io](http://www.draw.io): Online-Tool für Flowcharts usw. - aber eben auch UML
