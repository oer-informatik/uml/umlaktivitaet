## Übungsaufgabe UML Aktivitätsdiagramm: WebApp Seite zusammenstellen

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-aktivitaetsdiagramm-uebung-webapp</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/113691157190055022</span>

> **tl/dr;** _(ca. 6 min Lesezeit): Übungsaufgabe zu UML-Aktivitätsdiagrammen: Eine Website wird aufgerufen, Daten gesammelt und gerendert. Eine Beispiellösung wird am Ende der Seite gegeben._


Bevor Du diese Aufgabe versuchst zu lösen, solltest Du sicher stellen, dass Du mit den Notationmitteln des UML Aktivitätsidiagramms vertraut bist - z.B. in dem Du diesen [Artikel zum UML-Aktivitätsdiagramm](https://oer-informatik.de/uml-aktivitaetsdiagramm) liest.

### Aktivitäten beim Aufruf einer WebApp:

Es soll eine WebApp erstellt werden, die es den Besuchern des Kongresses ermöglicht, eine Übersicht der Sessions zu erhalten (und diese zu buchen, was aber nicht Bestandteil dieser Aufgabenstellung ist). Hierzu wurden in einem Brainstorming die folgenden Punkte notiert:

- Der Besucher gibt im Browser die URL der WebApp ein

- Der Browser sendet einen Request an die WebApp. 

- Die WebApp baut als Startseite ein Dashboard zusammen, dass aus mehreren Komponenten besteht.

- Die Webapp stellt eine Anfrage an die Datenbank zusammen, welche Sessions der Besucher als Favoriten markiert hat.

- Gleichzeitig stellt die Webapp eine Anfrage an die Datenbank zusammen, welche Sessions der Besucher gebucht hat.

- Gleichzeitig erstellt die WebApp einen Log-Eintrag über den Zugriff, den die WebApp selbst speichert (nicht in der Datenbank). Auf Beendigung des Loggens wird nicht gewartet.

- Auf jede der eingegangenen Anfragen antwortet die Datenbank einzeln mit den jeweiligen Datensätzen. 

- Wenn alle Antworten in der Webapp eingetroffen sind, wird ein Template geladen in Abhängigkeit von dem Gerät, von dem aus die WebApp aufgerufen wird:

- Falls es sich um ein Mobiltelefon handelt, wird das Mobiltelefon-Template geladen, andernfalls das Desktop-Template.

- Die gesamten Daten werden von der Webapp in das geladene Template integriert und an den Browser geschickt.

- Der Browser rendert die Seite und stellt sie dar.

Stelle für die oben genannten Aktivitäten ein UML-Aktivitätsdiagramm zusammen, aus dem neben dem Kontrollfluss auch die Verantwortungsbereiche hervorgehen. Erstelle zunächst nur den Kontrollfluss, der ohne den Datenfluss darzustellen.

Zu einfach? Dann nutze die verschiedenen Möglichkeiten der UML, um ergänzend zum Kontrollfluss den relevanten Datenfluss als Objekte oder Pin-notiert darzustellen. 

### Beispiellösung

Wer nach der Bearbeitung das eigene Ergebnis vergleichen will, der kann sich folgende Beispiellösung anschauen:

<button onclick="toggleAnswer('sessionbuchung')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="sessionbuchung">

![Darstellung der Nachrichtenablaufs beim Aufruf einer Buchungsseite über das MVP-Pattern](plantuml/aktivitaet-uebung-website-rendern.png)

Hinweis zur Lösung:
Bewertung mit 20 Punkten:

- 13 Aktivitäten: 13P

- FlowEnd: 1P

- Fork/Join: 3P

- Bedingung: 3P

Abzüge:
- keine Verantwortungsbereiche (Swimlanes) -2P

- keine / Falsche Pfeile -2P

- Aktivitäten als Rechteck (wie Objekte), also keine abgerundete Ecken -2P

- Guards (Bedingungen) nicht in eckigen Klammern -1P

- Zusammenführung von Decision nicht in Merge Node -1P

</span>

