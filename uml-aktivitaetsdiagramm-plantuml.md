## UML Aktivitätsdiagramm und die Modellierung mit plantUML

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-aktivitaetsdiagramm-plantuml</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/112066979165700523</span>

> **tl/dr;** _(ca. 6 min Lesezeit): Mithilfe von PlantUML lassen sich auch UML-Aktivitätsdiagramme per "Diagramm as Code" erstellen. Das geht nicht ganz so leicht von der Hand wie Sequenz- oder Klassendiagramme, da plantUML hier etwas umständlich ist. Für einfache Diagramme wird der Aufbau hier erklärt..._

Dieser Artikel ist Teil einer Artikelreihe über UML-Diagramme: [Klassendiagramm](https://oer-informatik.de/uml-klassendiagramm) / [Sequenzdiagramm](https://oer-informatik.de/uml-sequenzdiagramm) / [Aktivitätsdiagramm](https://oer-informatik.de/uml-aktivitaetsdiagramm) / [Zustandsdiagramm (State)](https://oer-informatik.de/uml-zustandsdiagramm) / [Anwendungsfall-Diagramm (Use Case)](https://oer-informatik.de/uml-usecase). Für alle sind jeweils auch PlantUML-Anleitungen verlinkt.

PlantUML bietet die Möglichkeit, UML-Diagramme als Code zu definieren und so einfach in die Dokumentationen der Repositories einzubinden. Man braucht dazu eigentlich nur einen Browser und einen Webservice wie [Planttext](https://www.planttext.com/) oder [PlantUML](https://www.plantuml.com/plantuml/uml/SoWkIImgAStDKL3tBIqkLiZCpKqjKaZCLN3CIqlCIrSeBadDICtZ0glYiWfEpYZAB2bHCBJcil8jpId9pCdCI-LI00AOZ9QMaPQMDL1zVW6NGsfU2j1a0000).

Die Markdown-Engines von Gitlab, Github, Bitbucket & Co. unterstützen PlantUML nativ und zeigen die Diagramme direkt an. Wenn man sich an den UML-Standard halten will, macht es einem PlantUML aber nicht immer einfach - und auch die Online-Referenz ([plantuml.com/de/activity-diagram-beta](https://plantuml.com/de/activity-diagram-beta)) geht sehr pragmatisch mit der UML um.

Im Folgenden werden einige Tipps gegeben, wie sich mit PlantUML ansehnliche und relativ standardkonforme Aktivitätsdiagramme darstellen lassen.

![Ein PlantUML-Aktivitätsdiagramm im Entwurfsskizzenstil mit Handschrift-Schriftbild](plantuml/11_aufhuebschen03.png)

### Ein einfaches unverzweigtes Aktivitätsdiagramm

Plantuml fügt die einzelnen Aktivitäten zwischen `start` und `stop` ein, wobei jede Aktivität mit Doppelpunkt beginnt und mit Semicolon endet: `:aktivitätsname;`:

```plantuml
@startuml 'Muss immer am Anfang stehen
start
:Schritt 1;
:Multiline
    Schritt **2**;
stop
@enduml
```

In diesem Beispiel ist das einfache Aktivitätsdiagramm um Kommentare ergänzt worden:

![UML Aktivitätsdiagramm - Simple Start Stop](plantuml/01_aktion_start_stop.png)

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/SoWkIImgAStDKL3tBIqkLiZCpKqjKaZCLN3CIqlCIrSeBadDICtZ0glYiWfEpYZAB2bHCBJcil8jpId9pCdCI-LI00AOZ9QMaPQMDL1zVW6NGsfU2j1a0000) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/01_aktion_start_stop.plantuml)
)

### Kontrollstrukturen: Verzweigung und Vereinigung

Kontrollstrukturen sind leider in PlantUML wenig intuitiv. Da wir die _Guards_ an den Kanten notieren wollen, müssen wir die Syntax etwas aufbohren. Von Haus schreibt PlantUML einen Text in die _decision node_ und zeichnet sie dann als Sechseck - das ist nicht im Standard enthalten. Ich löse das so: die erste Bedingung wird hinter `then()` in Klammern notiert, den `else`-Block sollte man in jedem Fall notieren:

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/SoWkIImgAStDuG8pk3BJD3GLIZ9IynGqejrJSr8K4akBYr8pau4YAR6QN0f0O9MTM9gMgX1Gb9zIcbrYpPMQKvmAL0sYW2g0wZBJW8Rb5t0v0Db0am40) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/03_einfacheBedingung.plantuml)
)

``` {style="plantuml"}
if() then ([Geld ausreichend])
    :kaufe Produkt;
else ([else])
endif
```

![Verzweigungen mit einfacher Bedingung im UML Aktivitätsdiagramm](plantuml/03_einfacheBedingung.png)



Beispiel mit Aktivitäten an beiden Alternativkanten (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/DSj12i9G30JGVKwHPNi5NGfUGbo8Vs83QNv9ultzVkXi1eOtMQyeKbbMhWtEo-FM6rYm6K82-bo9HovpVlVzK5C4_4gmnF3_Ej5ztdPO-_g74LamczQR2cNrumS0) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/03_einfacheVerzweigung.plantuml):
)

``` {style="plantuml"}
start
if() then ([Code fehlerfrei])
    :Code compilieren;
else ([else])
    :Fehlermeldung erstellen;
endif
stop
```

![Verzweigungen mit einfacher Bedingung im UML Aktivitätsdiagramm](plantuml/03_einfacheVerzweigung.png)

Nicht alle Bedingungen lassen sich unmittelbar als Prädikat formulieren.
Sofern nähere Eräuterungen nötig sind sieht die UML vor, dass diese über eine Notiz mit dem Stereotyp `<<decision input>>` erfolgt, die mit der *decision node* verbunden wird.


In PlantUML gibt es diese Möglichkeit nicht, daher muss man sich mit einer Notiz, die neben der vorigen Aktion steht, behelfen. Gemäß UML wird innerhalb der *decision node* nichts notiert - hier weicht die PlantUML-Dokumentation vom UML-Standard ab.

![Komplexere Verzweigungen im UML Aktivitätsdiagramm](plantuml/03_komplexereVerzweigung.png)

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/FKynRWCn3Dlr2et95v8W3R8L7JiM7HHRheLUvK2MKw3lkP_SnofuY1OHb4Y2b-vaFduMc00ExwcQk6EcHxCZW9JT7hsoukx3RV3d7hGv0yQSJfcJT6cAelVXv_EaNxjZ5JMM5BeykMjJyMrr3DTAcbbV86xuz3gyqIWFDZVwGhhzIlhcHO_0I-U8BXGxikVhtz1jJJLYhcpaGqEXDs7d48Ihb6ZJxd097DN-0G00) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/03_komplexereVerzweigung.plantuml)
)

``` {style="plantuml"}
:Schritt davor;

if() then ([true])
note right
    <<decision input>>
    Ist hinreichend
    Bonität vorhanden?
end note
    :Kaufvertrag abwickeln;
else ([false])
    :zunächst Beratung anbieten;
endif
```

Für mehrfache Verzweigungen können multiple Bedingungen als verschachtelte If-Statements modelliert werden:

![Mehrfache Verzweigungen im UML Aktivitätsdiagramm](plantuml/03_mehrfacheVerzweigung.png)

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/ROun3i8m301tly9Z-uLsAOWVm8OOejHDB8A371U-Zq4mKIy-oNUIfsP1RRqL-00CnvYLpN0EZweZyDBrQ9a4k_CfD2FSNNf0ds5FJqgh99GGCnQMQnk1IgF_vh3TACLityZTz_g9dAGgavBuMMPUe5czm-Jipxu0) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/03_mehrfacheVerzweigung.plantuml)
)

``` {style="plantuml"}
if() then ([Taste A])
    :Bewegung nach links;
elseif() then ([Taste D])
    :Bewegung nach rechts;
else ([else])
    :ignorieren;
endif
```

Mehrfache Verzweigungen können aber auch als Switch/Case-Statement umgesetzt werden:

![Mehrfache Verzweigungen im UML Aktivitätsdiagramm](plantuml/03_mehrfacheVerzweigungAngepasst.png)

Codebeispiel: (
  [online bearbeitbar](http://www.plantuml.com/plantuml/uml/SoWkIImgAStDuG8pk8hBCqkICnGqIbABI-mrkP8JYrCLD1IIy_DICaioy_CK71KqkHGKh49IAqeK3AqnvPqKD1NW2eomA716Ci0OYxmBahm95dbN97bJQwxKl1I8au7kpo_WSW3oWPu10000) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/03_mehrfacheVerzweigungAngepasst.plantuml)
)

``` {style="plantuml"}
switch ()
case ( [Taste A] )
 :Bewegung nach links;
case ( [Taste D] ) 
 :Bewegung nach rechts;
case ( [else] )
  :nur geradeaus fahren;
endswitch
```

### Wiederholungsstrukturen: kopf- und fussgesteuerte Schleifen

Mit PlantUML erfolgt die Modellierung eine fussgesteuerten Schleife mit einem `repeat / repeat while () is ([ `*guard* `])`-Block:

![Fussgesteuerte Schleifen im UML Aktivitätsdiagramm](plantuml/04_fussgesteuerteSchleife.png)

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/SoWkIImgAStDuG8pk8fI2r8JIxWKWC0gkAGqBLKX8pyvexWaDJCzDSyQAgrGdf6PawgWeQcGMQoW4PsQKvmQgycbQnUTdvIQfv2ObvwQdP-RcveNQmqqDR-0oo4rBmLe1G00) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/04_fussgesteuerteSchleife.plantuml)
)

``` {style="plantuml"}
start
repeat
    :Stufe hochsteigen;
repeat while () is ([else])
->[oben angekommen];
stop
```

Im Fall einer kopfgesteuerten Schleife wird die Bedingung zunächst geprüft, die zu wiederholenden Aktionen also ggf. nie ausgeführt.

![Kopfgesteuerte Schleifen im UML Aktivitätsdiagramm](plantuml/04_kopfgesteuerteSchleife.png)

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/SoWkIImgAStDuG8pkCepCdDIDJGLCeiLDA8pIlFIYpBJOpMv5830ogaelqIXE3aZDpErA3NF6aamjRWO992QbmBM3zHQd5XI0jHNN99VmEK050VQ0000) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/04_kopfgesteuerteSchleife.plantuml)
)

``` {style="plantuml"}
start
while() is ([hungrig])
    :Brot schmieren;
    :Brot essen;
endwhile ([satt])
stop
```

### Concurrency: Parallelisierung von Aktionen (Splitting und Synchronisation)

Im Gegensatz zu einem *decision node* oder *merge node* müssen bei Gabelungen (*fork node*) oder Synchronisierung (*join node*) an allen eingehenden Kanten ein Token anliegen, damit sie wiederum Token weiterreichen. Entsprechend werden an allen ausgehenden Kanten dann Token weitergereicht. In PlantUML wird dies durch einen `fork... fork again... end fork`-Block erreicht:

![Synchronisierung von UML Aktivitätsdiagrammen mit PlantUML](plantuml/05_concurrency.png)

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/SoWkIImgAStDuG8pkDBoYxAv5830oZSnEIDL8IKpjTWQBAgGc9wOcGUHqzFJAyrBKSW5AG6BfEUaPYPdLALcbcIcAwGa0hMa0kL1se3cvXTmEG2vG4i0) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/05_concurrency.plantuml)
)


``` {style="plantuml"}
start
fork
    :Mache dies;
fork again
    :...und mache
    gleichzeitig das;
end fork
stop
```

### Partitionen / Swimlanes zur Unterscheidung von Verantwortungsbereichen

Die UML sieht vor, dass modelliert werden kann, welche Akteurin oder welches System für bestimmte Aktionen verantwortlich oder organisatorisch Zuständig ist. Aufgrund des Aussehens werden diese Verantwortungsbereiche, die die UML *partition* nennt, oft *swimlanes* genannt. Diese Verantwortungsbereiche werden in PlantUML definiert, indem in der Zeile oberhalb einer Aktivität zwischen Pipes der Partitionsname steht (`|Partition|`). Das wird leider schnell etwas unübersichtlich, aber mit etwas Übung klappt das:

![Swimlanes per PlantUML](plantuml/06_swimlanes.png)

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/RP0nJiGm44Lxdy8TTo3D8Y4eG0XHq4-oV-n8PhAQ6NSv3iVOBXV3Sf1GnBhpqt__MpwPathzAk7kiUgKunj4ggJuWJb3U2a9aM5Ua21XVOPDUVjcDzwkMQ5hM9zeoZGwvjB4Td-fSiOQOZkz_K3kNyaSai4-_5Dxu9tBFEM_p8t9ddkGX77nI9KxE7YTZ6Gp578UZa_uDUsYsvNRyB3ZJwWh9QEn3PPgjdFpvH9EhQNzqWy0) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/06_swimlanes.plantuml)
)

``` {style="plantuml"}
'Durch Nennung Reihenfolge festlegen
|Geschäftsführer|
|Fachabteilung|
|Kunde|
    start
|Kunde|
    :Lastenheft;
|Fachabteilung|
    :Pflichtenheft;
|Geschäftsführer|
    :Angebot auf
    Pflichtenheft
    basierend;
|Kunde|
    :Angebotsprüfung;
    :Vertragsabschluss;
    stop
```

### Signale/ Ereignisse senden und empfangen; Objektflüsse darstellen

Wenn neben dem Kontrollfluss noch der Objektfluss in ein Diagramm einfließen soll, stößt PlantUML schnell an seine Grenzen. Es können Signale empfangen (`:empfange<`) und gesendet werden (`:sende>`). Ausserdem können mit Objekten Daten und der Kontrollfluß von einer Aktivität zur nächsten übergeben werden (`:übergebenes Objekt]`). Aber präziserte Notationen (Pin-Notation, Parametergruppen, Streams) sind leider mit PlantUML nicht möglich (oder mit nicht bekannt).

![Empfangende und gesendete Signale sowie Objektfluß](plantuml/07_signals.png)

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/FSmn3e9048NXVawnlU42gp5eHQCIfBLOsHSWWLcScTNewQNHLtvv-Kkr89RdYSXNRLiNzUNQ5WqU6MexyYUeXXwkGeWGjt775DytykVG3J9sWxad9CWTeu5TvaXktO7jbSJuVu9ya7lmbdm3NH8hreY28zmFzgIM5YfNpFFq1G00) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/07_signals.plantuml)
)

``` {style="plantuml"}
:HTTP-POST-Request<
:Message Header + Body]
:Nachricht verarbeiten und
    Antwortnachricht erzeugen;
:Response]
:sende Response>
stop
```

### Ablaufende

Wenn das Erreichen eines Endes zwar den aktuellen Ausführungsstrang beendet - also den eingetroffenen Token konsumiert - aber in der Gesamtaktivität noch weitere Token vorhanden sein können, muss ein Ablaufende mit einem einfachen `end` modelliert werden. 

![Ablaufende als Tokensenke](plantuml/08_ablaufende.png)

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/SoWkIImgAStDuG8pkDBoYxAv5830Ah4aDpNF6YoWa9YUc9a74Kv9pSl69bwScPSE50TpKlDIk81O0QG6Q6v-0PS3a0kq1000) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/08_ablaufende.plantuml)
)

``` {style="plantuml"}
start
fork
    :atmen;
fork again
    :denken;
fork again
    :singen;
    end
end fork
stop
```

### Konnektoren (Sprungmarken) zur übersichtlicheren Darstellung


Konnektoren lassen sich relativ einfach über geklammerte Sprungmarkennamen (`(A)`) erzeugen. Wenn danach keine ausgehenden Kanten dargestellt werden sollen, muss zusätzlich ein `detach` notiert werden:


![Konnektoren und Sprungmarken](plantuml/09_sprungmarken.png)

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/9Ox1pSCm28NlJC7jZm__0erKfRrtWYW7L9C6DNOYJEJDka0NgsDp0RsFnsD8Hcf7MA4EC6_w1YpLtLpjdesUIvFkhdAa5qciqkLHu1zwIZrmz5ZjoROT0CuBoZkJjLK9WHMpi6TjZaOnBx8oTWubOJTc5cET7FnVHphcvVjHZbCFP_fGCiglFm00) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/09_sprungmarken.plantuml)
)

``` {style="plantuml"}
start
fork
    (A)
    detach
    (B)
fork again
    :Mach' das;
end fork
stop


(A)
repeat
    :immer wieder das;
repeat while () is ([weiter])
->[aufhören];
(B)
```

### Vor- und Nachbedingungen

Aktionen können mit Vor- und Nachbedingungen versehen werden. Diese werden jeweils als Notiz mit dem jeweiligen Stereotyp notiert. Die Position kann rechts oder links erfolgen.


![Vor- und Nachbedingung](plantuml/10_vornachbedingung.png)

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/bP2nhi8m34NtV8N_uEcjxuMgbPWH4X9xcvWc8hMHu_1rRFmOIPaGBDmfoRtnaM-VTHGjIpBhmVmTN92e2h__XbWH9Cv13LHPczYDQI_ec7pKoDHrgxEDc16EB1FwI7EX6PQICzYfkxBOd-aP6qe-tMz1ck_R5u7XtCO0IZLIKiWOQSrkgbwedFKpTZUwyCwb-ler6Cb3MxfsmHVJrsijvW40) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/10_vornachbedingung.plantuml)
)

``` {style="plantuml"}
start
:Schritt 1;
note right
    <<localPrecondition>>
    Diese Vorbedingung muss <b>vor</b>
    Ausführung dieser Aktion erfüllt sein
    ====
    <<localPostcondition>>
    Diese Nachbedingung muss <b>nach</b>
    Ausführung dieser Aktion erfüllt sein
end note
stop
```

### plantUML-Webservice

PlantUML bietet einen Webservice, der Diagramme unmittelbar online rendert.

Variante 1: der PlantUML-Quelltext ist online als Resource verfügbar und soll gerendert ausgegeben werden. Hierzu muss eine URL nach dem Muster:

[http://www.plantuml.com/plantuml/proxy?fmt=AUSGABEFORMAT&src=https://URL\_PLANTUMLSOURCE](http://www.plantuml.com/plantuml/proxy?fmt=epstxt&src=https://gitlab.com/oer-informatik/uml/umlaktivitaet/-/raw/main/plantuml/11_aufhuebschen02.plantuml)

erstellt werden, wobei als *AUSGABEFORMAT* `png`, `svg`, `eps`, `epstext` und `txt` genutzt werden kann.


Variante 2: PlantUML kodiert den Quelltext, um ihn so über den Webservice zugänglich (und bearbeitbar) zu machen. Die URL ist in diesem Fall wie folgt aufgebaut:

[https://www.plantuml.com/plantuml/AUSGABEFORMAT/CODIERTERQUELLTEXT](https://www.plantuml.com/plantuml/png/ZL2z3jem49xnKvoOOEWBG8KeH9qeKg5BXHXEyP5OSUpqV2P2mBlN8QM5g9MQvUBFtyztNWP1bbXR5IhZ6cIi8QCJHYkVlPCjrWJZJSoDHjYZ2_3jqb3BHeZ7waNpAHTpDdAvAaZV2lEgO1-TNdlh6OBpRN_XMHFtO8PSxIqAhMoansrciFPE3zL5wOEqfP4trh-jrFsbsDOBNVyZUnYwCKKftydQrFp-jYcpR__kSllwvTQDZCwFkOv1V4tS7Hxld2PJGzZzp51hsD6FQBWc9_CNuSbpbSPlmqJUqTbGsv7-q7eYO876GK_3KC4pTQ566GdcfHhD2X9W2ertWJwgeN0c56J-1dUefiRwIuUJqwgFxG-_6WvMXETsq9R64aocO09C3huX1vAgXUDKVLWSJiQPK98-pbNlcImQjKo-iRO_0G00)

wobei als *AUSGABEFORMAT* `png`, `svg`, `eps`, `epstext` und `txt` genutzt werden kann. Wird als *AUSGABEFORMAT* `uml` gewählt erhält man den bearbeitbaren Quelltext.

Editierbare Links lassen sich auch über den Service von planttext.com erstellen, sie nutzen die selbe Quelltextcodierung und URLs nach dem Muster:

[https://www.planttext.com/?text=CODIERTERQUELLTEXT](https://www.planttext.com/?text=ZL2z3jem49xnKvoOOEWBG8KeH9qeKg5BXHXEyP5OSUpqV2P2mBlN8QM5g9MQvUBFtyztNWP1bbXR5IhZ6cIi8QCJHYkVlPCjrWJZJSoDHjYZ2_3jqb3BHeZ7waNpAHTpDdAvAaZV2lEgO1-TNdlh6OBpRN_XMHFtO8PSxIqAhMoansrciFPE3zL5wOEqfP4trh-jrFsbsDOBNVyZUnYwCKKftydQrFp-jYcpR__kSllwvTQDZCwFkOv1V4tS7Hxld2PJGzZzp51hsD6FQBWc9_CNuSbpbSPlmqJUqTbGsv7-q7eYO876GK_3KC4pTQ566GdcfHhD2X9W2ertWJwgeN0c56J-1dUefiRwIuUJqwgFxG-_6WvMXETsq9R64aocO09C3huX1vAgXUDKVLWSJiQPK98-pbNlcImQjKo-iRO_0G00)

Auch in den erzeugten PNG-Dateien wird der codierte Quelltext als Titel hinterlegt, so dass sie sich relativ einfach später weiterverarbeiten lassen.

### plantUML-Formatierung: Aufhübschen von Aktivitätsdiagrammen

Wenn die Diagramme erstmal stehen, will man sie aufhübschen. Dafür stehen allerlei möglichkeiten zur Verfügung, die v.a. auf der plantUML-Seite dargestellt werden. Einige Beispiele sind hier abgebildet:

Der Standard bei PlantUML ist mittlerweile recht ansehnlich. In früheren Versionen waren die Diagramme für meine Begriffe zu bunt, heute sehe ich fast keine Notwendigkeit, hier anzupassen:

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/NO-nRiCm34Jdy2iu9aD_m2c20itQeLc3349rYONOaYhI2GhuuojPCEfQoud4untv9cgI3dr7dD_u-J5IbYCUaxuFeg57AiK4Pas8CENJ66lvXE_1fHvUzul1QqZtfM66JtWuF0lqXrMGpSquNoaZTvv33xGvGirNIvlc1kK4YS4BNcarroWanw-x5h4BFwKFRwc6rcBUx_-1FD_6pzPruEsEdV3s728yQzvCBZjwEPolpZjfOIzxgfxP8lu2) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/11_aufhuebschen01.plantuml)
)

```{style="plantuml"}
@startuml
    |QM|
  |Dev|
  |Customer|
  start
  repeat
  :add Requirment;
  |Dev|
  fork
    |QM|
    :Review;
    |Dev|
    fork again
    :ask Customer>
	Detach
	:get response<
  fork again
  :test;
  fork again
  :deploy;
  end
 |Dev|
  end fork
   |Customer|
     repeat while () is ([open tasks])
->[finished];
|Dev|
stop
@enduml
```  

![Beispiel eines einfachen Aktivitätsdiagramms mit PlantUML](plantuml/11_aufhuebschen01.png)

Ich habe mir trotzdem angewöhnt, die Farben und Schriften anzpassen. Die meisten Diagramme, die ich hier verwende haben die folgenden `skinparams` gesetzt:

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/VL3DZjem43xZfnXnWaDV85I8GjGJHIhqKY4EetX8h3XsrXuJ8SAxhqC42z7kNZAJJz_FDrvuHiUXqiANojJei8AKZXWq_xQ6DrWHZDSXKn9XXyR3ltDDZLDCRYoUDByoLYV5vujOObRcpWOZLrPR1zxOHYoja-Hw84LNxZGoBNKWaIgih953LLvOpqsX-C7usa9SdufiBDFbcxnXq5hb1VydhMFBwyj5pjdcCuSsfLChshz_ucSLFDkAN2i0RYyPfwEQi5iIb1Asz1QKgyZmB4Az5k1eNHa7mCscMv8jdHGr7Uc3rXC1SrIcPw4luHuw5wEK6BD2Z9ASE0RxsXfFFyL0bp3v63z49TNQdZkSZ1IFqFZpwFTo8DnlWwPGcc0o1ULXihSr6U3Op1-cuiTyVrH6-OBaOIPwpsXHYqNq3PL-1m00) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/11_aufhuebschen02.plantuml)
)

```{style="plantuml"}
@startuml
skinparam DefaultFontName "Lucida Sans Typewriter"

skinparam Activity{
BackgroundColor snow
BorderColor DarkSlateBlue
DiamondBackgroundColor ghostwhite
DiamondBorderColor DarkSlateBlue

}
skinparam Note{
BorderColor DarkSlateBlue
BackgroundColor LightYellow
}

skinparam ArrowColor DarkSlateBlue
|Dev|
|QM|
|Customer|
start
...
stop
@enduml
```

![Beispiel eines Aktivitätsdiagramms mit den Skins, wie ich sie hier nutze](plantuml/11_aufhuebschen02.png)

Wenn man den Entwurfscharakter hervorheben will, ist vielleicht die `handwritten`-Option geeignet. Mit eine Handschrift-Schrift (die im PlantUML-System hinterlegt sein muss) sieht das ganz ansehnlich aus. Ich nutze hier lokal "FG Virgil" (der Webservice kenn diese Schrift jedoch nicht und ersetzt sie). Ist natürlich Geschmackssache. 

Codebeispiel: (
  [online bearbeitbar](https://www.plantuml.com/plantuml/uml/VP71Qjn038RFcQSGEIM7li2cX7HZqaiRQ0A54d8G7gr7U3noDVAQ9UwxTpOcMzUqkLYszElxV-6RRAWsTD7bbbEFYXrKjCCXsfqakyUEuFpkCtndRJYUBrG1an-LpIY1wK2BKIT9wg3IqNhIOzrYGuzsY0HAjIrcEQ2NaLC3EuoPt6BqgJRUinrUtBO06fKX-Lk9ef2JZ6uhwaddHeNQFaOqsiRYN36MC7wzrGJ9DWQsa-B_2FTh4UHUZ5xUyLiRVU4cs0-AKSR2MLwaAkE_25D5-ybDtxwMn-sGJJhIoG6y_gTIbNh2uyi6lOS7-ZcmTfJighJcNO2TQ5iAm2i6e8WVQCyq7aL_PBCGi45Eimfp2s-cr-wi8iCwkBDDGmPAkPUKwQDRxMsCSh5VTptrKGx7FYNlJgRbuvJlhmFXxJOO0qU2YqlW31TFqbC2AydoywNxSFsquyGva7--SZEp87ftKxX35ty3) /
  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlaktivitaet/raw/main/plantuml/11_aufhuebschen03.plantuml)
)

 ```{style="plantuml"}
@startuml
' Welche Schriften gibt es auf dem System?
' listfonts als plantUML-Kommando gibt's aus.
skinparam DefaultFontName "FG Virgil"
skinparam handwritten true
skinparam monochrome true
skinparam packageStyle rect
skinparam shadowing false

|Dev|
|QM|
|Customer|
start
...
stop
@enduml
```  
![PlantUML Ansicht im Entwurfsskizzenstil mit Handschrift-Schriftbild](plantuml/11_aufhuebschen03.png)

## Links und Referenzen

[Projektwebsite https://www.plantuml.com/](https://www.plantuml.com/),

Website, auf der direkt plantUML-Quelltexte geparst werden können: <https://www.planttext.com/>
